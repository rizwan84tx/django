var table = document.querySelector('#gametable');
var tableCell = table.querySelectorAll('#cell');

//***querySelectorAll***//
// [].forEach.call(
//     document.querySelectorAll(css_selector),
//     (var){
//         console.log(var);
//     }
// );

[].forEach.call(tableCell, (i)=>{
  i.addEventListener('click',function(){
    i.textContent = "X";
  });
  i.addEventListener('dblclick',function(){
    i.textContent = "O";
  });
});
