// myvariable.textContent - This returns just the text
var mainHead = document.querySelector('h2');
mainHead.textContent = "DOM - Content Change";

// myvariable.innerHTML - This returns the actual html
var para = document.querySelector('p');
para.innerHTML = "<strong>Refer below information</strong>"

// myvariable.getAttribute() - This returns the original attribute
var specialId = document.querySelector('#special');
console.log(specialId);
var specialIdAnchor = specialId.querySelector('a');
console.log(specialIdAnchor);
var specialIdLink = specialIdAnchor.getAttribute('href');
console.log(specialIdLink);

// myvariable.setAttribute() - This allowed you to set an attribute
specialIdAnchor.setAttribute('href','https://www.flipkart.com')
var specialIdLink = specialIdAnchor.getAttribute('href');
console.log(specialIdLink);
