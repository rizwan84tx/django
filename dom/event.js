var headHover = document.querySelector('#hover');
var headClick = document.querySelector('#click');
var headDouble = document.querySelector('#double');

//Change content and color when mouse is over object
headHover.addEventListener('mouseover',function(){
  headHover.textContent = "Mouse is On";
  headHover.style.color = "Red";
})
//reset the content when mouse is out of the object
headHover.addEventListener('mouseout',function(){
  headHover.textContent = "HOVER";
  headHover.style.color = "Black";
})

//Change content and color when mouse is single clicked
headClick.addEventListener('click',function(){
  headClick.style.color = "Blue";
})

//Change content and color when mouse is double clicked
headDouble.addEventListener('dblclick',function(){
  headDouble.innerHTML = "<strong><em>DOUBLE-CLICKED</em></strong>"
  headDouble.style.color = "Grey"
})
