var head = document.querySelector("h1"); //grab the header
// head.style.color = 'red'; //changing color

var colorList = ['Blue','Green','Brown','Red'];

function getColor(){
  var colorPop =  colorList.pop();
  colorList.splice(0,0,colorPop); //inserting the pop'd color to index 0 
  return colorPop;
}

function changeColor(){
    var colorInp = getColor();
    head.style.color = colorInp;
}
setInterval("changeColor()",500);
