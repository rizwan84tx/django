class Book():
    def __init__(self,title,author,pages):
        self.title = title
        self.author = author
        self.pages = pages

    #String representation
    def __str__(self):
        return "{} written by {}".format(self.title,self.author)

    #Custom length representation
    def __len__(self):
        return self.pages

    def __del__(self):
        print ("Book Sold-Out")

mybook = Book("Python","Rizwan",100)

#String representation
print(mybook)
#Custom length representation
print(len(mybook))
#Delete the object instance
del mybook
