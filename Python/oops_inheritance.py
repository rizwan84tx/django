#Inheritance
#Code reuse & reduction of complicated programming
class Animal():

    def __init__(self):
        print ("Animal is created")

    def eat(self):
        print ("Eating")

    def whoAmi(self):
        print ("Animal")

#Derivative Class inheriting Main Class
class Dog(Animal):

    def __init__(self):
        print ("Dog Created")

    def bark(self):
        print("Woof Woof")

# Main class instance
myanimal = Animal()
myanimal.eat()

#Derivative Class Instance
mydog = Dog()
#Using main class methd via inherited derivative class
mydog.whoAmi()
mydog.eat()
mydog.bark()
