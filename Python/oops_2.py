class Circle():

    pi = 3.14

    def __init__(self,radius):
        self.radius = radius

    def area(self):
        return self.radius*self.radius*Circle.pi

    def set_radius(self,new_rad):
        self.radius = new_rad

myshape = Circle(10)

print(myshape.radius)
print(myshape.area())

myshape.set_radius(5)

print(myshape.radius)
print(myshape.area())
