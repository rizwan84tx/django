class Dog():

    #Class obj attr
    species = "mammal"

    def __init__(self,breed,name):
        self.breed = breed
        self.name = name

# Instance of Obj Class "Dog"
mydog = Dog("Lab","Sammy")

print (mydog.breed)
print (mydog.name)
print (mydog.species)
