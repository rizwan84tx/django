from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

# def index(request):
#     return HttpResponse("hello world!")
def index(request):
    my_dict = {'insert_me':"Hello from views.py in template/first_app!!"}
    return render(request,'first_app/index.html',context=my_dict)
