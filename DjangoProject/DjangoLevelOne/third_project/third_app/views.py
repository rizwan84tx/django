from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return HttpResponse("<h1> Welcome to Third APP </h1>")

def help(request):
  disp = {'help': "hello i'm there to help you!!"}
  return render(request,'help.html',context=disp)
