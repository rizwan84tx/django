from django.shortcuts import render
from django.http import HttpResponse
from appOne.models import AccessRecord,Webpage,Topic

# Create your views here.
def index(request):
    weblist = AccessRecord.objects.order_by('date')
    date_dict = {'access_records': weblist}
    display = {'display_content': "Display from appOne!!"}
    # return render(request,'appOne/index.html',context=display)
    return render(request,'appOne/index.html',context=date_dict)
