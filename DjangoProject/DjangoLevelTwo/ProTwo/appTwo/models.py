from django.db import models

# Create your models here.
class Name(models.Model):
    username = models.CharField(max_length=30)

    def __str__(self):
        return self.username

class Email(models.Model):
    name = models.ForeignKey('Name', on_delete = models.CASCADE)
    email = models.EmailField(max_length=50)

    def __str__(self):
        return self.email
