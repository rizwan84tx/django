from django.shortcuts import render
from django.http import HttpResponse
from appTwo.models import Name,Email
# Create your views here.
def index(request):
    return HttpResponse("<h1>Welcome!!!</h1><br><h3>Go to '/users' to view user data</h3>")

def users(request):
    name_list = Email.objects.order_by('name')
    name_dict = {'user_info':name_list}
    return render(request,'appTwo/users.html',context=name_dict)
