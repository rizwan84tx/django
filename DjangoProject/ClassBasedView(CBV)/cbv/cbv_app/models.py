from django.db import models

# Create your models here.
class Home(models.Model):
    name = models.CharField(max_length=256)
    location = models.CharField(max_length=256)
    owner = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class Members(models.Model):
    name = models.CharField(max_length=256)
    age = models.PositiveIntegerField()
    home = models.ForeignKey(Home,
                            related_name='members',
                            on_delete = models.CASCADE)

    def __str__(self):
        return self.name
