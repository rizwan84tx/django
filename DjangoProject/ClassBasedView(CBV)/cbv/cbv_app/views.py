from django.shortcuts import render
from django.views.generic import View
from django.views.generic import TemplateView, ListView, DetailView
from cbv_app import models

# Create your views here.
class IndexView(TemplateView):
    template_name='index.html'

class HomeListView(ListView):
    model = models.Home #(returns context as home_list)

class HomeDetailView(DetailView):
    model = models.Home #(returns context as home)
    template_name = 'cbv_app/home_detail.html'
