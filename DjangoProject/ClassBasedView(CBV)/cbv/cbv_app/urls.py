from django.urls import path, re_path
from cbv_app import views

app_name = 'cbv_app'

urlpatterns = [
    path('',views.HomeListView.as_view(),name='list'),
    re_path(r'^(?P<pk>[-\w]+)/$', views.HomeDetailView.as_view(),name='detail')
]
