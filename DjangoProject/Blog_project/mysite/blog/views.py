from django.contrib.auth import authenticate,login,logout
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View,TemplateView,CreateView,ListView,DetailView,UpdateView,DeleteView
from django.contrib.auth.forms import UserCreationForm


# Create your views here.
class Index(TemplateView):
    template_name = 'blog/index.html'

class Register(TemplateView):
    template_name = 'register.html'

class UserCreation(CreateView):
    form_class = UserCreationForm
    fiels = ('username', 'password')
    template_name = 'new.html'

    def get_success_url(self):
        return reverse('blog:thankyou')

    # def form_valid(self, form):
    #     return super(UserCreation,self).form_valid(form)

class ThankYou(TemplateView):
    template_name = 'thankyou.html'

class Login(TemplateView):
    template_name = 'login.html'

    def post(self,request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("Account not active")
        else:
            return HttpResponse("Invalid Credentials")

class Logout(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('index'))
