# Generated by Django 2.1.2 on 2019-01-12 17:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='name',
        ),
        migrations.DeleteModel(
            name='Person',
        ),
    ]
