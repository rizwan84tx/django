from django.urls import path
from blog import views

app_name = 'blog'

urlpatterns = [
    path('register/', views.Register.as_view(), name='register'),
    path('new/', views.UserCreation.as_view(), name='new'),
    path('thankyou/', views.ThankYou.as_view(), name='thankyou'),
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),
]
