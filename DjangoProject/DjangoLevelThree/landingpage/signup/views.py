from django.shortcuts import render
from django.http import HttpResponse
from signup.models import User
from signup import forms

# Create your views here.
def landing(request):
    return render(request,'landing.html')

def signup(request):
    signup_form = forms.UserForm()

    if request.method == "POST":
        signup_form = forms.UserForm(request.POST)

        if signup_form.is_valid():
            signup_form.save(commit=True)
            return landing(request) # takes to home landing page
    else:
        signup_form = forms.UserForm()

    return render(request,'signup.html',{'form':signup_form})
