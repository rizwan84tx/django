from django import forms
from django.core import validators

def check_name_lenght(name):
    if len(name) < 2:
        raise forms.ValidationError("**** Please input correct name ****")

class FormName(forms.Form):
    name = forms.CharField(validators=[check_name_lenght])
    email = forms.EmailField()
    confirm_email = forms.EmailField()
    text = forms.CharField(widget=forms.Textarea)

    def clean(self):
        if (self.cleaned_data.get('email') !=
            self.cleaned_data.get('confirm_email')):

            raise forms.ValidationError("EMAIL not Matching!!")

        return self.cleaned_data
