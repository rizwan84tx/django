var head = $('h1');
//normal styling
head.css('color','Blue');
head.css('background','grey');

//Object based styliing
var x = $('h2');
var styler = {
  color: 'white',
  background: 'blue',
}
x.css(styler);

//Default style is applied to all items
var listItem = $('li');
listItem.css('color','red')

//Modify only specific item - 'eq(x): x-index value'
listItem.eq(2).css('background','black')

//Modify the page content
//changed the header
head.text("JQuery for Beginners") //changes only text
x.html("<em>Welcome</em>") //Changed text and html design

//Event handling
$('h1').click(function (){
  console.log('Clicked');
})
//this - denotes the object that was selected in below case its all 'li' objects
$('li').click(function(){
  $(this).text('changed by click')
})

//Key press
$('input').eq(0).keypress(function(){
  $('h3').css('color','green');
})

//event
//'which' - numeric code of keyboard keys
//13 - enter key
$('input').eq(0).keypress(function(){
  if(event.which === 13) {
    $('h3').css('color','brown');
  }
})
//ON method
$('h1').on('mouseenter',function(){
  $(this).css(styler);
})

//Animation
$('input').eq('1').on('click',function(){
  $('h1').fadeOut(3000)
})

$('input').eq('1').on('dblclick',function(){
  $('h2').slideUp(3000)
})
