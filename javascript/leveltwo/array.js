var color = ["Red","Green","Blue"];
console.log(color);
//mixed datatype
var mixDataType = ["true", 100, "hello"];
console.log(mixDataType);
num = mixDataType[1]
len = mixDataType.length
console.log(num,len);
mixDataType[1] = 500
console.log(mixDataType);
// poping item from array
var num = [1,2,3,4,5];
console.log(num);
lastNum = num.pop()
console.log("Pop "+lastNum);
console.log(num);
// append array
num.push(10);
console.log("Append array");
console.log(num);
// Array in array
nestedArr = [[1,2,3],["a","b","c"],["true","false"]];
l=nestedArr.length;
lastSubArr = nestedArr[l-1];
console.log(nestedArr);
console.log("lenght: "+l);
console.log("Last Sub array :"+lastSubArr);

//Array interation
arr = ["A","B","C"];
for (var i=0; i<arr.length; i++){
  console.log(arr[i]);
}
//for-of method
for (item of arr){
  console.log(item);
}
//for-each method
//to call function for every array element
function hello(item) {
  console.log("hello "+item);
}
family = ["Rizwan","Sana","Yahya","Hafsa"];
family.forEach(hello)
