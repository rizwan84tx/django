//Problem #1//
//Display the length of the employee name
console.log("PROBLEM #1 : Display the length of the employee name");
var emp = {
  name: "Mohammed Rizwan",
  Job: "Engineer",
  age: 33,
  nameLength: function () {
    len = this.name.length;
    console.log("Name lenght of " + this.name + " is "+ len );
  }
}
emp.nameLength()

//Problem #2//
//Browser alerts  object pairs
console.log("PROBLEM #2 : Browser alerts object pairs");
var emp = {
  name: "Mohammed Rizwan",
  Job: "Engineer",
  age: 33
}
alert("Name is "+emp['name']+" , "+"Job is "+emp['Job']+" and "+"age is "+emp['age'])

//Problem #3//
//Display last name to console
console.log("PROBLEM #3 : Display last name to console");
var emp = {
  name: "Mohammed Rizwan",
  Job: "Engineer",
  age: 33
}
var empName = emp['name'].split(" ");
var lastName = empName[empName.length-1];
console.log(lastName);
