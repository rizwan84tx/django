function hello(){
  console.log("hello world");
}

function helloName(name) {
  console.log("hello "+name);
}

function addNum(num1,num2){
  console.log(num1+num2);
}

// Default parameters
function helloDefname(name="Ghost") {
  console.log("hello "+name);
}

// Return variable
function formal(name="Sam", title="Sir"){
  return title+" "+name;
}

// Local scope variable
function timesFour(num) {
  var result = num * 4;
  return result;
}

//Global scope variable
var v = "GLOBAL VAR"
function test(v) {
  console.log(v);
  v = "REASSIGNED VAR in FUNC"
  console.log(v);
}

test(v);
console.log(v);
