// JS Object formal
// {key1 : "value one", key2 : "value two"};
var carInfo = {make:"Hyundai",year:2011,model:"i10"};
console.log(carInfo);

//calling key value pair
var a=carInfo["make"];
var b=carInfo["model"];
console.log(a+" "+b);

//nested or mixed object
var myObj = {a:"hello",b:[1,2,3],c:{inside:['a','b']}}
console.log(myObj);

var x=myObj['a'];
//o/p - "hello"
var y=myObj['b'][1];
//o/p - 2
var z=myObj['c']['inside'][0];
//o/p - 'a'

console.log(x+" "+y+" "+z);

//Changing value
console.log(carInfo);
carInfo['year'] = 2015
console.log(carInfo);
carInfo['year'] += 2
console.log(carInfo);

//interation
//for-in method
for (keyInfo in carInfo) {
  console.log(keyInfo); //print key
  console.log(carInfo[keyInfo]); //print value of the key
}

//Object-method
//function inside a object
var myObject ={
  prop:"hello",
  myMethod: function(){
    console.log("I'm inside method");
  }
}
//Calling the method inside an Object
var inside = myObject.myMethod()
console.log(inside);

//simple obj-method example
var welcome = {
  name:"Rizwan",
  greet:function(){
    console.log("Welcome " + this.name);
  }
}
//"this" is used to call a key inside a method
welcome.greet()
